﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    public class Program
    {

        static void Main(string[] args)
        {
            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");
            string valor = Console.ReadLine();
            try
            {
                Console.WriteLine(Add(valor));
            }
            catch (NegativeException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static int Add (string numbers)
        {
            char customDelimiter = ',';
            int value = 0;
            if (!String.IsNullOrEmpty(numbers) && (numbers.IndexOf("//") > -1
                || numbers.IndexOf("\n") > -1))
            {
                customDelimiter = numbers.IndexOf("//") > - 1 ? 
                    numbers[numbers.IndexOf("//") + 2] : customDelimiter;
                numbers = numbers.Replace("//" + customDelimiter, "");
                numbers = CheckGoodSintaxi(numbers,customDelimiter.ToString());

            }

            if (String.IsNullOrEmpty(numbers))
            {
                return value;
            }
            else if (numbers != "error")
            {
                bool raiseException = false;
                List<int> negativeNumbers = new List<int>();
                string[] intList = numbers.Split(customDelimiter);
                foreach (string number in intList)
                {
                    int plusNumber = int.Parse(number);
                    if (plusNumber < 0)
                    {
                        negativeNumbers.Add(plusNumber);
                        raiseException = true;
                    }
                    else
                        value += plusNumber;
                }

                if (raiseException)
                {
                    NegativeException ex = new NegativeException(negativeNumbers);
                    throw ex;
                }
                else
                    return value;
            } else
            {
                throw new Exception();
            }
            
        }

        private static string CheckGoodSintaxi(string numbers, string customDelimiter)
        {
            numbers = numbers.Replace("\\", "");
            if (numbers.IndexOf(",\\n") > -1)
                return "error";
            else
            {
                numbers = numbers.Replace("\n", customDelimiter);
                if(numbers.IndexOf(",,") > -1)
                {
                    return "error";
                } else if(customDelimiter[0] == numbers[0])
                {
                    return numbers.Substring(1,numbers.Length-1);
                } else
                {
                    return numbers;
                }
            }
        }
    }
}
