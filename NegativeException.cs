﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    public class NegativeException : Exception
    {
        private string message;

        public NegativeException()
        {
        }

        public NegativeException(List<int> negativeNumbers)
        {
            message = "Negatives not allowed: ";
            for(int i = 0; i < negativeNumbers.Count; i++)
            {
                message += negativeNumbers[i].ToString() + " ";
            }
        }

        public override string ToString()
        {
            return this.message;
        }
    }
}
